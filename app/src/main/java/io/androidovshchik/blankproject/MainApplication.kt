package io.androidovshchik.blankproject

import android.content.Context
import com.github.androidovshchik.core.utils.context.appContext
import com.github.androidovshchik.core.utils.context.isDebug
import com.github.androidovshchik.support.BaseSApplication

@Suppress("unused")
class MainApplication : BaseSApplication() {

    override fun onCreate() {
        super.onCreate()
        /*
        int pixel = 0xff283441;
        System.out.println("pixel " + pixel);
        for (int i = 0; i < 80; i++) {
            pixel = pixel - 0x0a000000;
            System.out.println("> " + (pixel));
            System.out.println(">> 0x" + Integer.toHexString(pixel));
            System.out.println(">>> " + (pixel >>> 24));
            System.out.println("----------------------------------------");
        }
        */
        if (isDebug()) {
            Class.forName("com.facebook.stetho.Stetho")
                .getDeclaredMethod("initializeWithDefaults", Context::class.java)
                .invoke(null, appContext)
        }
    }
}