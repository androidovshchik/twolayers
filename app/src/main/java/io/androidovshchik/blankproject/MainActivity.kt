package io.androidovshchik.blankproject

import android.os.Bundle
import com.github.androidovshchik.support.BaseV7Activity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseV7Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        GlideApp.with(applicationContext)
            .asGif()
            .load(R.raw.var1)
            .into(background)
    }
}
