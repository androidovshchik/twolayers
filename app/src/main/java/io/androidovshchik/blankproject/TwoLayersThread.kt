package io.androidovshchik.blankproject

import android.annotation.SuppressLint
import android.graphics.Canvas
import android.view.SurfaceHolder

class TwoLayersThread(private val threadSurfaceHolder: SurfaceHolder,
                      private val rectThreadSurfaceView: TwoLayersView) : Thread() {

    var isRunning = false

    @SuppressLint("WrongCall")
    override fun run() {
        while (isRunning) {
            var canvas: Canvas? = null
            try {
                canvas = threadSurfaceHolder.lockCanvas(null)
                synchronized(threadSurfaceHolder) {
                    if (canvas != null) {
                        rectThreadSurfaceView.onDraw(canvas)
                    }
                }
            } finally {
                if (canvas != null) {
                    threadSurfaceHolder.unlockCanvasAndPost(canvas)
                }
            }
        }
    }
}