package io.androidovshchik.blankproject

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.SurfaceHolder
import android.view.SurfaceView
import timber.log.Timber
import android.graphics.Bitmap
import android.media.ThumbnailUtils

class TwoLayersView : SurfaceView {

    private lateinit var thread: TwoLayersThread

    private var bitmap: Bitmap? = null

    private val surfaceCallback = object : SurfaceHolder.Callback {

        override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {
            val input = BitmapFactory.decodeResource(resources, R.drawable.var1)
            val size = Math.min(width, height)
            val thumb = ThumbnailUtils.extractThumbnail(input, size, size)
            try {
                if (bitmap?.isRecycled == false) {
                    bitmap?.recycle()
                    bitmap = null
                }
                val output = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
                val canvas = Canvas(output)
                if (width > height) {
                    canvas.drawBitmap(thumb, (width - height) / 2f, 0f, null)
                } else {
                    canvas.drawBitmap(thumb, 0f, (height - width) / 2f, null)
                }
                if (bitmap?.isRecycled == false) {
                    bitmap?.recycle()
                    bitmap = null
                }
                bitmap = output.copy(Bitmap.Config.ARGB_8888, true)
            } finally {
                input.recycle()
                thumb.recycle()
            }
        }

        override fun surfaceCreated(holder: SurfaceHolder) {
            thread = TwoLayersThread(getHolder(), this@TwoLayersView)
            thread.isRunning = true
            thread.start()
        }

        override fun surfaceDestroyed(holder: SurfaceHolder) {
            var retry = true
            thread.isRunning = false
            while (retry) {
                try {
                    thread.join()
                    retry = false
                } catch (e: Exception) {
                    Timber.e(e)
                }
            }
        }
    }

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle)

    override fun onFinishInflate() {
        super.onFinishInflate()
        setZOrderOnTop(true)
        isFocusable = true
        holder.setFormat(PixelFormat.TRANSLUCENT)
        holder.addCallback(surfaceCallback)
    }

    public override fun onDraw(canvas: Canvas) {
        if (bitmap != null) {
            synchronized(bitmap!!) {
                canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR)
                if (bitmap != null) {
                    canvas.drawBitmap(bitmap!!, 0f, 0f, null)
                }
            }
        }
    }

    @Suppress("DEPRECATION")
    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        synchronized(bitmap!!) {
            when (event.action) {
                MotionEvent.ACTION_DOWN, MotionEvent.ACTION_MOVE -> {
                    subtractAlpha(event.x, event.y)
                }
            }
            return true
        }
    }

    private fun subtractAlpha(eventX: Float, eventY: Float) {
        val r = 20
        var x = eventX.toInt()
        var y = eventY.toInt()
        val offset = Math.abs(bitmap!!.width - bitmap!!.height) / 2
        if (bitmap!!.width > bitmap!!.height) {
            if (x < offset + r) {
                x = offset + r
            }
            if (x + r >= bitmap!!.width - 1 - offset) {
                x = bitmap!!.width - 1 - offset - r
            }
            if (y < r) {
                y = r
            }
            if (y + r > bitmap!!.height - 1) {
                y = bitmap!!.height - 1 - r
            }
        } else {
            if (x < r) {
                x = r
            }
            if (x + r > bitmap!!.width - 1) {
                x = bitmap!!.width - 1 - r
            }
            if (y < offset + r) {
                y = offset + r
            }
            if (y + r > bitmap!!.height - 1 - offset) {
                y = bitmap!!.height - 1 - offset - r
            }
        }
        for (i in (y - r) until (y + r)) {
            var j = x
            while ((j - x) * (j - x) + (i - y) * (i - y) <= r * r) {
                if (Color.alpha(bitmap!!.getPixel(j, i)) > 0) {
                    bitmap!!.setPixel(j, i, bitmap!!.getPixel(j, i) - 0x05000000)
                }
                j--
            }
            j = x + 1
            while ((j - x) * (j - x) + (i - y) * (i - y) <= r * r) {
                if (Color.alpha(bitmap!!.getPixel(j, i)) > 0) {
                    bitmap!!.setPixel(j, i, bitmap!!.getPixel(j, i) - 0x05000000)
                }
                j++
            }
        }
    }

    public override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        holder.removeCallback(surfaceCallback)
        if (bitmap?.isRecycled == false) {
            bitmap?.recycle()
        }
        bitmap = null
    }
}